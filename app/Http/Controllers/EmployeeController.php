<?php

namespace App\Http\Controllers;

use App\Models\Employee;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function indexView()
    {
        return view("employees.index");
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $draw = intval($request->draw);
        $start = intval($request->start);
        $length = intval($request->length);
        $columns = $request->columns;
        $order = $request->order;
        $search = $request->search;
        $search = $search['value'];

        $col = '';
        $dir = "";

        $valid_columns = [];

        for ($i = 0; $i < count($columns); $i++) {
            if ($columns[$i]['data'] != "action") {
                $valid_columns[] = $columns[$i]['data'];
            }
            if (!empty($order)) {
                if ($order[0]['column'] == $i) {
                    $col = $columns[$i]['data'];
                    $dir = $order[0]['dir'];
                }
            }
        }

        if ($dir != "asc" && $dir != "desc") {
            $dir = "desc";
        }

        $query = DB::table("employees");

        if ($order != null) {
            $query->orderBy($col, $dir);
        }

        if (!empty($search)) {
            $x = 0;
            foreach ($valid_columns as $term) {
                if ($x == 0) {
                    $query->where($term, "LIKE", "%$search%");
                } else {
                    $query->orWhere($term, "LIKE", "%$search%");
                }
                $x++;
            }
        }
        $query->offset($start)->limit($length);
        $employees = $query->get();
        $data = [];
        foreach ($employees as $rows) {
            $id = $rows->id;
            $data[] = [
                "name" => $rows->name,
                "position" => $rows->position,
                "office" => $rows->office,
                "age" => $rows->age,
                "start_date" => date('d-m-Y', strtotime($rows->start_date)),
                "salary" => number_format($rows->salary, 2),
                "action" => '<button onclick="editTable(' . $id . ')"  data-id="' . $id . '" class="btn btn-sm btn-warning mr-1">Edit</button>
                 <button onclick="deleteRow(' . $id . ')" data-id="' . $id . '" class="btn btn-sm btn-danger mr-1">Delete</button>'
            ];
        }

        $total_employees = $this->totalEmployees();
        $output = array(
            "draw" => $draw,
            "recordsTotal" => $total_employees,
            "recordsFiltered" => $total_employees,
            "data" => $data
        );

        return response()->json($output);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $id = $request->id;
            if ($id) {
                $this->update($request, $id);

                return response()->json([
                    "error" => false,
                    "msg" => "Data updated!"
                ]);
            } else {
                $employee = new Employee();
                $employee->name = $request->name;
                $employee->position = $request->position;
                $employee->office = $request->office;
                $employee->age = $request->age;
                $employee->salary = $request->salary;
                $employee->start_date = date("Y-m-d", strtotime($request->startDate));
                $employee->save();

                return response()->json([
                    "error" => false,
                    "msg" => "Data saved!"
                ]);
            }
        } catch (\Exception $e) {
            return response()->json([
                "error" => true,
                "msg" => $e->getMessage()
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $employee = Employee::where("id", "=", $id)
            ->selectRaw("
                id
                ,name
                ,position
                ,office
                ,age
                ,salary
                ,start_date
            ")
            ->first();
        return response()->json([
            "row" => $employee
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employee = Employee::where("id", "=", $id)->first();
        if ($employee) {
            $employee->name = $request->name;
            $employee->position = $request->position;
            $employee->office = $request->office;
            $employee->age = $request->age;
            $employee->salary = $request->salary;
            $employee->start_date = date("Y-m-d", strtotime($request->startDate));
            $employee->save();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Employee::where("id", "=", $id)->delete();
            return response()->json([
                "error" => false,
                "msg" => "Data deleted successfully!"
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "error" => true,
                "msg" => $e->getMessage()
            ]);
        }
    }
}
