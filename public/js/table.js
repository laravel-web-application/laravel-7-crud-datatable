function editTable(id) {
    $("#alertModal").toggleClass("display-none");
    $("#alertModal").addClass("display-none").removeClass("alert-danger")
    $("#inputId").val(id)
    $("#datatableModal").modal({
        backdrop: 'static',
        keyboard: false
    });
    $.ajax({
        url: window.app + "/datatable/" + id,
        dataType: 'json', // data type
        type: 'GET',
        success: function (data, textStatus, jqXHR) {
            $("#inputName").val(data.row.name);
            $("#inputPosition").val(data.row.position);
            $("#inputOffice").val(data.row.office);
            $("#inputAge").val(data.row.age);
            $("#inputSallary").val(data.row.salary);
            var date = moment(data.row.start_date, "YYYY-MM-DD").format("DD-MM-YYYY")
            $("#inputStartDate").val(date);
        }
    });
}

// Delete row in datatable
function deleteRow(id) {
    var swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
        },
        buttonsStyling: false
    })

    swalWithBootstrapButtons.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Yes, delete it!',
        cancelButtonText: 'No, cancel!',
        reverseButtons: true
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: window.app + '/datatable/' + id,
                type: 'DELETE',
                dataType: 'JSON',
                success: function (data) {
                    if (data.error) {
                        $("#alertHome").toggleClass("display-none").toggleClass("alert-success");
                        $("#alertMsgHome").html(data.msg);
                        setTimeout(function () {
                            $("#alertHome").addClass("display-none").removeClass("alert-success")
                        }, 3000);
                    } else {
                        swalWithBootstrapButtons.fire(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                        )
                        $("#datatable").DataTable().ajax.reload();
                    }
                }
            });
        } else if (
            /* Read more about handling dismissals below */
            result.dismiss === Swal.DismissReason.cancel
        ) {
            swalWithBootstrapButtons.fire(
                'Cancelled',
                'Your imaginary file is safe :)',
                'error'
            )
        }
    })
}

$(document).ready(function () {
    // initializing Datatable
    var table = $("#datatable").DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            url: window.app + "/datatable",
            type: "GET",
        },
        "columns": [{
            data: "name",
            name: "name"
        },
            {
                data: "position",
                name: "position"
            },
            {
                data: "office",
                name: "office"
            },
            {
                data: "age",
                name: "age"
            },
            {
                data: "start_date",
                name: "start_date"
            },
            {
                data: "salary",
                name: "salary"
            },
            {
                data: "action",
                name: "action",
                orderable: false
            },
        ]
    });

    // Display modal for add new data
    $("#btnNewEmployee").click(function (e) {
        e.preventDefault();
        $("#alertModal").addClass("display-none").removeClass("alert-danger")
        $("#inputId").val(null)
        $("#datatableModal").modal({
            backdrop: 'static',
            keyboard: false
        });
    });

    // Pikaday
    var datepicker = $('.datepicker').pikaday({
        firstDay: 1,
        format: 'D-MM-YYYY',
    });

    // When submit the form
    $('#formEmployee').submit(function (e) {
        e.preventDefault()
        var form = $(this),
            url = form.attr("action")
        var formData = form.serialize();
        $.ajax({
            url: window.app + "/datatable",
            data: formData,
            dataType: 'json', // data type
            type: 'POST',
            success: function (data, textStatus, jqXHR) {
                if (data.error) {
                    $("#alertModal").toggleClass("display-none").toggleClass("alert-danger");
                    $("#alertMessage").html(data.msg);
                    setTimeout(function () {
                        $("#alertModal").addClass("display-none").removeClass("alert-danger")
                    }, 3000);
                } else {
                    $("#alertHome").toggleClass("display-none").toggleClass("alert-success");
                    setTimeout(function () {
                        $("#alertHome").addClass("display-none").removeClass("alert-success")
                    }, 3000);
                    $("#alertMsgHome").html(data.msg);
                    $("#datatableModal").modal("hide");
                    $("#datatable").DataTable().ajax.reload();
                }
            }
        });
    });

    // Reset form when close
    $('#datatableModal').on('hidden.bs.modal', function () {
        $('#datatableModal form')[0].reset();
    });

    function deleteItem(id) {

    };
});
