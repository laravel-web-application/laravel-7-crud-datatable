@extends('layouts.app')

@section('style')
    <link href="{{ asset('css/pikaday.css') }}" rel="stylesheet">
    <style type="text/css">
        .display-none {
            display: none;
        }

        .display-block {
            display: block;
        }

        .btn {
            margin: 0.1rem 0.2rem;
        }
    </style>
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex">
                            <div class="p-2">Dashboard</div>
                            <div class="ml-auto p-2">
                                <!-- Button trigger modal -->
                                <button type="button" id="btnNewEmployee" class="btn btn-primary btn-sm text-right">
                                    New Employee
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <div id="alertHome" class="show display-none alert alert-dismissible fade" role="alert">
                            <span id="alertMsgHome"></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="table-responsive">
                            <!-- DataTable -->
                            <table id="datatable" class="table">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Position</th>
                                    <th>Office</th>
                                    <th>Age</th>
                                    <th>Start date</th>
                                    <th>Salary</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="datatableModal" tabindex="-1" role="dialog"
             aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLongTitle">Employee</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{-- alert-success show --}}
                        <div id="alertModal" class="show alert alert-dismissible fade" role="alert">
                            <span id="alertMessage"></span>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <form id="formEmployee" method="POST" action="{{ url('/datatable') }}">
                            <input type="hidden" name="id" id="inputId">
                            <div class="form-group row">
                                <label for="inputName" class="col-md-3 col-form-label">Name</label>
                                <div class="col-md-9">
                                    <input required name="name" type="text" class="form-control" id="inputName"
                                           placeholder="Name">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputPosition" class="col-md-3 col-form-label">Position</label>
                                <div class="col-md-9">
                                    <input required name="position" type="text" class="form-control" id="inputPosition"
                                           placeholder="Position">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputOffice" class="col-md-3 col-form-label">Office</label>
                                <div class="col-md-9">
                                    <input required name="office" type="text" class="form-control" id="inputOffice"
                                           placeholder="Office">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputAge" class="col-md-3 col-form-label">Age</label>
                                <div class="col-md-5">
                                    <input required name="age" type="number" class="form-control" id="inputAge"
                                           placeholder="Age">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputStartDate" class="col-md-3 col-form-label">Start Date</label>
                                <div class="col-md-9">
                                    <input required name="startDate" type="text" class="form-control datepicker"
                                           id="inputStartDate" placeholder="Start Date">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="inputSallary" class="col-md-3 col-form-label">Sallary</label>
                                <div class="col-md-9">
                                    <input required name="salary" type="number" class="form-control" id="inputSallary"
                                           placeholder="Sallary">
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" form="formEmployee" id="submitBtn" class="btn btn-primary">Save changes
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('js/moment.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script src="{{ asset('js/pikaday.js') }}"></script>
    <script src="{{ asset('js/pikaday.jquery.js') }}"></script>
    <script src="{{ asset('js/table.js?v=').time() }}"></script>
@endsection
